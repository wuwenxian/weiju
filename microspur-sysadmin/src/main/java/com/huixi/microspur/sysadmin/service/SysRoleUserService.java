package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.entity.SysRoleUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
public interface SysRoleUserService extends IService<SysRoleUser> {

}
