package com.huixi.microspur.web.entity.VO;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 *  根据条件分页查询时 接受前端传过来的值
 * @Author 叶秋
 * @Date 2020/2/26 22:15
 * @param
 * @return
 **/
@Data
@ApiModel(value="按条件分页查询的VO类")
public class ListPageAppealVO {

    @ApiModelProperty(value = "查询人id，判断是否点赞")
    private String userId;

    @ApiModelProperty(value = "页数")
    private Integer pageNO;

    @ApiModelProperty(value = "每页数量")
    private Integer pageSize;

    @ApiModelProperty(value = "点赞数")
    private Integer endorseCount;

    @ApiModelProperty(value = "评论数")
    private Integer commentCount;

    @ApiModelProperty(value = "浏览量")
    private Integer browseCount;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;



}
