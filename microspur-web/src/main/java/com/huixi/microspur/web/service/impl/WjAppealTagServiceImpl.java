package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.appeal.WjAppealTag;
import com.huixi.microspur.web.mapper.WjAppealTagMapper;
import com.huixi.microspur.web.service.WjAppealTagService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 诉求-对应标签 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjAppealTagServiceImpl extends ServiceImpl<WjAppealTagMapper, WjAppealTag> implements WjAppealTagService {

    @Override
    public List<WjAppealTag> listByAppealTag(String appealId) {

        QueryWrapper<WjAppealTag> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("appeal_id",appealId);
        List<WjAppealTag> list = list(objectQueryWrapper);


        return list;
    }
}
