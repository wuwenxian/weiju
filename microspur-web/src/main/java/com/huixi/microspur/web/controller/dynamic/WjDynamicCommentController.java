package com.huixi.microspur.web.controller.dynamic;


import com.huixi.microspur.commons.base.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 动态评论表 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@RestController
@RequestMapping("/wjDynamicComment")
public class WjDynamicCommentController extends BaseController {

}

